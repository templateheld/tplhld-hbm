<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hbm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g&Y@ylW%nXqNWu?qVPDb>7CLV)L9Etm!^HSP$9e~TE |uz#yi[J- ].!<jnOIHLJ' );
define( 'SECURE_AUTH_KEY',  'H,DAxPm!Pd$ ln(B0<tX9ab zc?bddLF(YAF}Md 7pLcYmwn~5ulSf3^;Vk{ml/Z' );
define( 'LOGGED_IN_KEY',    'yy~qMVKT1u}xKbO6>?|sKVdxaU~A7<=x+:MWMj5/iRhZuR2Iouvw-A{nt|9,/l0d' );
define( 'NONCE_KEY',        '=^Lt=Ec8$[h{-IEgCs!1x9+58b`XGrtI.v7S)g:837-XrE^a7S&dh~l/|A*5% :~' );
define( 'AUTH_SALT',        'v(@?0L,;nDw-W-<0l~giKJh;RXt`zQ hUpp#}=>_kjI1r7x6e?EC8`J[RU(KSeog' );
define( 'SECURE_AUTH_SALT', '1dJwN%:[]nlU;aO,Kqv-7+.kT<OX* ue)GgB?DdZOpk%K.KGvo}.&4H8^cEe|kF0' );
define( 'LOGGED_IN_SALT',   '_A-K5jZ$ALI{YFFhO@{H~L KM&kvR#,!B)pbG9^/qPNTb8+V0d*# dS1l$=UNu5;' );
define( 'NONCE_SALT',       'f78gOwNvO(OQg;3L@SRXmZT/ LBx=-GaeAihFfP^5/+4OcT_{P9Cxgd&5ecNtA}I' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
