<?php /* Template Name: pageLeistungen */

get_header(); ?>

    <section id="teaser-ir">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        Unsere Leistungen im Überblick
                    </h1>
                </div>
            </div>
        </div>
    </section>


    <section id="services-grid">
        <div class="container">
            <div class="row">

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Industriereinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/industriereinigung.png" alt="Industriereinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/industriereinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Baureinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/baureinigung.png" alt="Baureinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/baureinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Solar- /Photovoltaikreinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/solar-photovoltaikreinigung.png" alt="Solar- /Photovoltaikreinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/photovoltaikreinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Trockeneisreinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/trockeneisreinigung.png" alt="Trockeneisreinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/trockeneisreinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Unterhaltsreinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/unterhaltsreinigung.png" alt="Unterhaltsreinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/unterhaltsreinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

                <div class="card col-12 col-lg-4">
                    <h5 class="card-title">Glasreinigung</h5>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/glasreinigung.png" alt="Glasreinigung">
                    <div class="card-body mx-auto">
                        <a href="<?php echo get_site_url(); ?>/glasreinigung" class="btn btn-info">Mehr erfahren</a>
                    </div>
                </div>

            </div>
        </div>

    </section>


<?php get_footer(); ?>