<?php /* Template Name: Unterseite */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section id="teaser-ir">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        <?php echo the_field('titel'); ?>
                    </h1>
                        <?php echo the_field('untertitel'); ?>
                </div>
            </div>
        </div>
    </section>

<section id="ir-main-pic">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 mr-auto ml-auto">
                 <img class="img-fluid" src="<?php echo the_field('hauptbild'); ?>" alt="Leistungen">
            </div>
         </div>
    </div>
</section>

<?php if( get_field('kleines_bild') != NULL ): ?>
<section id="summary">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 ml-auto">

                <h1><?php the_field('titel'); ?></h1>

                    <?php echo the_content(); ?>

            </div>
            <div class="col-12 col-lg-5 mr-auto">
                <?php if( get_field('kleines_bild_2') != NULL ): ?>
                    <div class="row">
                        <figure class="figure col-6">
                            <img src="<?php echo the_field('kleines_bild'); ?>" class="figure-img img-fluid">
                            <figcaption class="figure-caption"><?php echo the_field('caption'); ?></figcaption>
                        </figure>

                        <figure class="figure col-6">
                            <img src="<?php echo the_field('kleines_bild_2'); ?>" class="figure-img img-fluid">
                            <figcaption class="figure-caption"><?php echo the_field('caption_2'); ?></figcaption>
                        </figure>
                    <?php if( get_field('kleines_bild_3') != NULL ): ?>
                        <figure class="figure col-12">
                            <img src="<?php echo the_field('kleines_bild_3'); ?>" class="figure-img img-fluid">
                            <figcaption class="figure-caption"><?php echo the_field('caption_3'); ?></figcaption>
                        </figure>
                    <?php endif; ?>
                    </div>
                <?php else: ?>
                    <?php if( get_field('caption') != NULL ): ?>
                        <div class="row">
                            <figure class="figure col-6 mx-auto">
                                <img src="<?php echo the_field('kleines_bild'); ?>" class="figure-img img-fluid">
                                <figcaption class="figure-caption"><?php echo the_field('caption'); ?></figcaption>
                            </figure>
                        </div>
                    <?php else: ?>
                        <img class="img-fluid" src="<?php echo the_field('kleines_bild'); ?>">
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php else: ?>
<section id="summary">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7 mx-auto">

                <h1><?php echo the_field('titel'); ?></h1>

                <?php echo the_content(); ?>

            </div>
        </div>
    </div>
</section>
<?php endif; ?>


<?php endwhile; ?>
<?php get_footer(); ?>