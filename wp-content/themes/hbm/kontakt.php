<?php /* Template Name: Kontakt */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section id="teaser-ir">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        <?php echo the_field('titel'); ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>

<section id="ir-main-pic">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 mr-auto ml-auto">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d690.0105466028605!2d10.843285914966998!3d48.26864229439451!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479ea1ea5f247cd1%3A0x85721f8a377e7051!2sHans-Sachs-Stra%C3%9Fe+8A%2C+86399+Bobingen!5e0!3m2!1sde!2sde!4v1520260760002" width="100%" height="462" frameborder="0" style="border:0" allowfullscreen></iframe>         </div>
            </div>
</section>


    <section id="summary">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 ml-auto">

                    <h1><?php the_field('titel'); ?></h1>

                    <?php echo the_content(); ?>

                </div>
                <div class="col-12 col-lg-5 mr-auto">
                    <div class="row">

                <?php if( have_rows('bilder') ): ?>
                    <?php while ( have_rows('bilder') ) : the_row(); ?>
                            <figure class="figure col-6">
                                <img src="<?php echo the_sub_field('image'); ?>" class="figure-img img-fluid">
                                <figcaption class="figure-caption"><?php echo the_sub_field('caption'); ?></figcaption>
                            </figure>
                    <?php endwhile; ?>

                <?php endif; ?>
                    </div>
                </div>
    </section>

<?php endwhile; ?>
<?php get_footer(); ?>