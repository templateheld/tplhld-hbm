<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>HBM - easy clean</title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <a class="navbar-brand col-1 offset-1" href="http://dev.templateheld.de"><img src="<?php echo get_template_directory_uri(); ?>/img/hbm-logo.svg" alt="HBM"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-hbm">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar-hbm">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo get_site_url(); ?>">Startseite <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url(); ?>/ueber-unser-unternehmen">Unternehmen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url(); ?>/leistungen">Leistungen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_site_url(); ?>/stellenangebote">Stellenangebote</a>
                </li>
            </ul>
        </div>
    </nav>
</header>