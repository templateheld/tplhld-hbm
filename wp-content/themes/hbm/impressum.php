<?php /* Template Name: Impressum */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section id="teaser-ir">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        <?php echo the_field('titel'); ?>
                    </h1>
                        <?php echo the_field('untertitel'); ?>
                </div>
            </div>
        </div>
    </section>

<section id="ir-main-pic">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10 mr-auto ml-auto">
            </div>
         </div>
    </div>
</section>

<?php if( get_field('kleines_bild') != NULL ): ?>
<section id="summary">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 ml-auto">

                <h1><?php the_field('titel'); ?></h1>

                    <?php echo the_content(); ?>

            </div>
            <div class="col-12 col-lg-5 mr-auto">
                <img class="img-fluid" src="<?php echo the_field('kleines_bild'); ?>">
            </div>
        </div>
    </div>
</section>
<?php else: ?>
<section id="summary">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7 mx-auto">



                <?php echo the_content(); ?>

            </div>
        </div>
    </div>
</section>
<?php endif; ?>


<?php endwhile; ?>
<?php get_footer(); ?>