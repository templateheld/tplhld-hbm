<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>

<?php if( get_the_ID() != 109 && get_the_ID() != 124 && get_the_ID() != 123 && get_the_ID() != 119 && get_the_ID() != 165 && get_the_ID() != 152 && get_the_ID() != 181 && get_the_ID() != 161 && !is_404() ): ?>

<section id="interest">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-lg-5 question">

                <?php
                if (is_home()):
                    echo '<h2>Haben wir Ihr Interesse geweckt?</h2>';
                else:
                    $title = get_the_title();
                    echo '<h2>Sie brauchen eine <strong>'.$title.'</strong>?</h2>';
                endif;
                ?>

                <h3>Treten Sie mit uns in Kontakt.</h3>
            </div>

            <div class="col-12 col-lg-3">
                <a href="<?php echo get_site_url(); ?>/kontaktformular" class="btn btn-info">Kontaktieren</a>
            </div>
        </div>
    </div>
</section>

<?php else: ?>
    <section id="interest">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-12"></div>
            </div>
        </div>
    </section>
<?php endif; ?>

<section id="suggestions">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-5">
                <a href=<?php echo get_site_url(); ?>/leistungen">
                <img src="<?php echo get_template_directory_uri(); ?>/img/leistungen.svg" alt="Leistungen">
                <h2>Leistungen</h2>
                <p>Sie wollen wissen womit sich unser Unternehmen ausseinandersetzt? Sehen Sie hier alle unsere Leistungen auf einem Blick.</p>
                </a>
            </div>
            <div class="col-12 col-lg-5">
                <a href="<?php echo get_site_url(); ?>/stellenangebot">
                <img src="<?php echo get_template_directory_uri(); ?>/img/stellenangebote.svg" alt="Stellenangebote">
                <h2>Stellenangebote</h2>
                <p>Sie sind auf der Suche nach einem Job? Dann schauen Sie sich doch unsere Stellenangebote an.</p>
                </a>
            </div>
        </div>
    </div>
</section>


<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-1 offset-lg-1">
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hbm-logo.svg" alt="HBM"></a>
            </div>
            <div class="col-12 col-lg-4 offset-lg-2">
                <div class="row">
                    <div class="col-6 col-lg offset-lg-3">
                        <h2>Kontakt</h2>
                        <p>Adresse<br>Hans-Sachs-Straße 8A<br>86399 Bobingen<br></p>
                    </div>
                    <div class="col-6 col-lg">
                        <h2>&nbsp</h2>
                        <p>Telefon<br>Mehmet Akpinar<br>0172 / 93 90 152<br><br>Hasan Önce<br>0172 / 56 23 102</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col-12 col-lg">
                        <h2>Unternehmen</h2>
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>/ueber-unser-unternehmen">Über uns</li></a>
                            <li><a href="<?php echo get_site_url(); ?>/kontakt">Kontakt</li></a>
                            <li><a href="<?php echo get_site_url(); ?>/stellenangebote">Stellenangebote</li></a>
                        </ul>
                    </div>
                    <div class="col-12 col-lg">
                        <h2>&nbsp;</h2>
                        <ul>
                            <li><a href="<?php echo get_site_url(); ?>/datenschutz">Datenschutz</a></li>
                            <li><a href="<?php echo get_site_url(); ?>/impressum">Impressum</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-slim.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

<?php wp_footer(); ?>

</body>
</html>
