// Requires
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');

// Variables
var path = '',
    scss = path + 'sass/**/*.scss',
    css = path + '',
    js = path + 'js',
    jsFile = '/main.js';

// Tasks development
// SASS compile and minify
gulp.task('sass', function() {
  return gulp.src(scss)
    .pipe(sass({
      outputStyle: 'compressed'
    }))  // Converts SASS to CSS via gulp-sass
    .pipe(gulp.dest(css))
})

// JS minify
gulp.task('minijs', function() {
  return gulp.src(js + jsFile)
    .pipe(minifyJS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(js))
})

gulp.task('init', function() {
  runSequence(['sass', 'minijs']);
})

// Gulp watch
gulp.task('default', function() {
  gulp.watch(scss, ['sass']);
  gulp.watch(js + jsFile, ['minijs']);
})
