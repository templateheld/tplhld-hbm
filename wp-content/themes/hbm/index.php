<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>


<section id="teaser">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg text-center">
                <h1>
                    HBM easy clean
                </h1>
                <h2>
                    Ihr Partner für professionelle Reinigung.
                </h2>
                <p>
                    Um Unternehmen in einer stetig schneller werdenden Wirtschaft von Aufgaben zu entlasten, die nicht in ihr Kerngeschäft fallen, übernimmt <span style="font-family: robotobold; color: #386CD2">HBM easy clean</span> viele Aufgaben, die um und an einer Immobilie und Industrie sowie in eineim Betrieb entstehen können.                </p>
                <a href="<?php echo get_site_url(); ?>/kontaktformular" class="btn btn-info">Kontaktieren</a>
            </div>
        </div>
    </div>
</section>


<section id="services-grid">
    <div class="container">
        <div class="row">

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Industriereinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/industriereinigung.png" alt="Industriereinigung">
                <div class="card-body mx-auto">
                    <a href="industriereinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Baureinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/baureinigung.png" alt="Baureinigung">
                <div class="card-body mx-auto">
                    <a href="baureinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Solar- /Photovoltaikreinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/solar-photovoltaikreinigung.png" alt="Solar- /Photovoltaikreinigung">
                <div class="card-body mx-auto">
                    <a href="solarphotovoltaikreinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Trockeneisreinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/trockeneisreinigung.png" alt="Trockeneisreinigung">
                <div class="card-body mx-auto">
                    <a href="trockeneisreinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Unterhaltsreinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/unterhaltsreinigung.png" alt="Unterhaltsreinigung">
                <div class="card-body mx-auto">
                    <a href="unterhaltsreinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

            <div class="card col-12 col-lg-4">
                <h5 class="card-title">Glasreinigung</h5>
                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/glasreinigung.png" alt="Glasreinigung">
                <div class="card-body mx-auto">
                    <a href="glasreinigung" class="btn btn-info">Mehr erfahren</a>
                </div>
            </div>

        </div>
    </div>

</section>

<section id="services-toolbar" class="d-none d-lg-block">
    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/industriereinigung">Industrie-<br>reinigung<br></a>
            </div>
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/baureinigung">Bau-<br>reinigung<br></a>
            </div>
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/trockeneisreinigung">Trockeneis-<br>reinigung<br></a>
            </div>
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/unterhaltsreinigung">Unterhalts-<br>reinigung<br></a>
            </div>
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/glasreinigung">Glas-<br>reinigung<br></a>
            </div>
            <div class="col">
                <a href="<?php echo get_site_url(); ?>/solarphotovoltaikreinigung">Solar- / Photovoltaik-<br>reinigung<br></a>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();
