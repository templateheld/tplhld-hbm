<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Templateheld
 */

get_header(); ?>

    <section id="teaser-ir">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1>
                        404 Not Found
                    </h1>
                    <h2>
                        Diese Seite wurde nicht gefunden.
                    </h2>
                    <p>
                        Da hat sich wohl jemand vertippt...
                    </p>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();
